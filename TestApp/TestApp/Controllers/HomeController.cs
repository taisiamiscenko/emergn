﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using TestApp.Data;
using TestApp.Models;

namespace TestApp.Controllers
{    
    /// <summary>
    /// Home controller
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Retrieves categories 
        /// </summary>
        private ICategoryHelper iCategoryHelper;

        public HomeController(ICategoryHelper categoryHelper)
        {
            iCategoryHelper = categoryHelper;
        }

        public HomeController() : this(new CategoryHelper())
        {            
        }

        /// <summary>
        /// Renders and returns the Index page
        /// </summary>
        /// <returns>Index page</returns>
        public ActionResult Index()
        {            
            var categoryList = iCategoryHelper.GetCategorySelectList();
            IndexModel model = new IndexModel() { Categories = categoryList, SelectedCategory = new Category() };
            return View(model);
        }

        [HttpGet]
        public JsonResult GetCategoryImages(string categoryName)
        {            
            var imageList = iCategoryHelper.GetCategoryImageList(categoryName);
            
            return Json(imageList, JsonRequestBehavior.AllowGet);
        }
    }
}
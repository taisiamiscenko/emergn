﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestApp.Models
{
    /// <summary>
    /// Represents an image 
    /// </summary>
    public class Image
    {
        /// <summary>
        /// ID
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// URL
        /// </summary>
        public string URL { get; set; }
        /// <summary>
        /// Source URL
        /// </summary>
        public string SourceUrl { get; set; }
    }
}
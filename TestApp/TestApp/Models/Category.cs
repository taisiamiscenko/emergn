﻿using System.ComponentModel.DataAnnotations;

namespace TestApp.Models
{
    /// <summary>
    /// Represents a category
    /// </summary>
    public class Category
    {
        /// <summary>
        /// ID
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        [Display(Name="Category Name")]
        public string Name { get; set; }
    }
}
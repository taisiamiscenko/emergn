﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace TestApp.Models
{
    /// <summary>
    /// Represents a list of categories received via API call
    /// </summary>
    public class IndexModel
    {
        /// <summary>
        /// List of categories
        /// </summary>
        public IEnumerable<SelectListItem> Categories { get; set; } 

        /// <summary>
        /// Represents the selected category object
        /// </summary>
        public Category SelectedCategory { get; set; }
    }
}
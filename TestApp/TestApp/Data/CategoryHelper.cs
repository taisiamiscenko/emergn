﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using TestApp.Models;

namespace TestApp.Data
{
    /// <summary>
    /// Category data 
    /// </summary>
    public class CategoryHelper : ICategoryHelper
    {
        #region constants
        /// <summary>
        /// Category list URL
        /// </summary>
        private const string CATEGORY_URL = "CategoryURL";
        /// <summary>
        /// Category image list URL
        /// </summary>
        private const string IMAGE_URL = "ImageURL";
        /// <summary>
        /// Page count
        /// </summary>
        private const int PAGE_COUNT = 20;
        #endregion

        #region public
        /// <summary>
        /// Gets category list from API call
        /// </summary>
        /// <returns>category list</returns>
        public IEnumerable<SelectListItem> GetCategorySelectList()
        {
            //Get all categories
            string url = ConfigurationManager.AppSettings[CATEGORY_URL];
            List<Category> categories = new List<Category>();
            categories.Add(new Category() { ID = 0, Name = "" });
            categories.AddRange(GetCategories(url));

            //Create a selectable list
            IEnumerable<SelectListItem> selectList =
                from c in categories
                select new SelectListItem
                {
                    Text = c.Name,
                    Value = c.ID.ToString()
                };
            
            return selectList;

        }

        public IEnumerable<Image> GetCategoryImageList(string categoryName)
        {
            string url = ConfigurationManager.AppSettings[IMAGE_URL];
            url = url.Replace("{page_count}", PAGE_COUNT.ToString()).Replace("{category_name}", categoryName);
            List<Image> images = GetImages(url);
            return images;
        }
        #endregion

        #region private
        /// <summary>
        /// Retrieves category list
        /// </summary>
        /// <param name="url">url to call</param>
        /// <returns>Category list</returns>
        private List<Category> GetCategories(string url)
        {
            List<Category> list = new List<Category>();
            try
            {
                XDocument doc = GetXMLDoc(url);

                var query = from data in doc.Descendants("category")
                            select new Category
                            {
                                ID = Convert.ToInt32(data.Elements("id").First().Value),
                                Name = data.Elements("name").First().Value,
                            };

                list = query.ToList();                
            }
            catch (Exception e)
            {
                //TODO log an error from e.Message
            }

            return list;
        }
        /// <summary>
        /// Retrieves category image list
        /// </summary>
        /// <param name="url">url to call</param>
        /// <returns>Image list</returns>
        private List<Image> GetImages(string url)
        {
            List<Image> list = new List<Image>();
            try
            {
                XDocument doc = GetXMLDoc(url);

                var query = from data in doc.Descendants("image")
                            select new Image
                            {
                                ID = data.Elements("id").First().Value,
                                URL = data.Elements("url").First().Value,
                                SourceUrl = data.Elements("source_url").First().Value,
                            };

                list = query.ToList();
            }
            catch (Exception e)
            {
                //TODO log an error from e.Message
            }

            return list;
        }

        /// <summary>
        /// Retrieves response data as an XML document 
        /// </summary>
        /// <param name="url">URL</param>
        /// <returns>XDocument</returns>
        private XDocument GetXMLDoc(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            request.Method = "GET";

            XDocument doc;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    doc = XDocument.Load(stream);
                }
            }
            return doc;

        }
        #endregion
    }
}
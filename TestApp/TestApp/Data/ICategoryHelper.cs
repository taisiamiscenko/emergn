﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TestApp.Models;

namespace TestApp.Data
{
    public interface ICategoryHelper
    {
        IEnumerable<SelectListItem> GetCategorySelectList();
        IEnumerable<Image> GetCategoryImageList(string categoryName);
    }
}

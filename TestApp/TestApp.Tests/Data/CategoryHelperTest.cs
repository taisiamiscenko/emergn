﻿using TestApp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestApp;
using TestApp.Controllers;
using TestApp.Models;

namespace TestApp.Data.Tests
{
    [TestClass()]
    public class CategoryHelperTest
    {
        private CategoryHelper helper; 

        [TestInitialize()]
        public void Prepare()
        {
            // Arrange
            helper = new CategoryHelper();
        }

        [TestMethod()]
        public void GetCategorySelectListTest()
        {
            //TODO: the test does not work due to attempts to get a value from Web.config. Needs mocking of ConfigurationManager
            
            // Act
            IEnumerable<SelectListItem> items = helper.GetCategorySelectList();

            // Assert
            Assert.IsNotNull(items);
            Assert.IsTrue(items.Count() > 0);
        }

        [TestMethod()]
        public void GetCategoryImageListTest()
        {
            //TODO: the test does not work due to attempts to get a value from Web.config. Needs mocking of ConfigurationManager

            string categoryName = "boxes";

            // Act
            IEnumerable<Image> items = helper.GetCategoryImageList(categoryName);

            // Assert
            Assert.IsNotNull(items);
            Assert.IsTrue(items.Count() > 0);
        }
    }
}


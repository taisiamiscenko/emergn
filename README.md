# Introduction
My name is Taisija Miscenko-Slatenkova.
This is a project created for testing my development skills at Emergn 

# 1. How long did you spend on the coding test?
It took about an hour to think of the solution and about 2.5 hours of coding 

# 2. What would you change / implement in your application given more time?
- Tests
- Index page: make a scrollable image list, show image names, etc.  
- CSS classes to make the page look better
- place javascript code in a separate file  

# 3. Did you use IOC? Please explain your reasons either way.
(If IOC stands for Inversion of Control)
I used DependencyInjection: HomeController has a field of type ICategoryHelper which is passed in the constructor.
The purpose is to decouple these two classes and make their development and testing more independent.

# 4. How would you debug an issue in production code?
In most systems it is impossible to debug without stopping a system for realtime customers, unless we can move customers to other part of cluster and debug a single node (and we are lucky then).
Necessity to debug in production is extraordinary, so I would carefully read logs and deploy another release with heavy logging.
If you mean not production but just the code of the last release, then I would make stubs to emulate external system (if necessary), 
if possible, take the database snapshot from live system, and then debug it locally (reconfige the system to use stubs and local DB).

# 5. What improvements would you make to the cat API?
I would introduce page_number parameter to http://thecatapi.com/api/images/get call.
For http://thecatapi.com/api/categories/list would be handy to have some filtering options and page_size and page_number.

# 6. What are you two favourite frameworks for .Net?
ASP.NET, EntityFramework

# 7. What is your favourite new feature in .Net?
Despite being not too new, LINQ, because it works with any collection. From the newest features I've read about, but never used, is ClickOnce technology. 
It offers great flexibility in updating new version of the application. 

# 8. Describe yourself in JSON.
var me = 
{
  name: "Taisija", 
  surname: "Micsenko-Slatenkova",
  age: 35, 
  city: "Riga",
  state: "Latvija",
  education: "PhD in Computer Science",
  married: true,
  children: [
    {name: "Mark", age: 7},
    {name: "Maija", age: 2}
  ],
  hobbies: ["reading", "travelling", "learning new things"]
};

